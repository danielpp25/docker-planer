#!/bin/bash
# Script creacion de imagen NodeJS API planer

echo "Api Planer creation"

npm install
docker build -t planer .
docker run -d -p 3000:3000 planer

echo "Ingrese al descriptor swagger aquí"
echo "http://localhost:3000/explorer"