***** Previamente debe tener instalado npm *****

Para crear el contenedor localmente y ejecutarlo  corra install.sh
o siga los siguientes pasos:
1. Descargue las dependencias "npm install"
2. Compile docker build -t planer .
3. Ejecute docker run -d -p 3000:3000 planer

*************************************************

Si desea ejecutar la imagen creada.

docker run -p 3000:3000 kaarrlloz/planer:tag
