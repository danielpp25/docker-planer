
FROM mhart/alpine-node:latest 

WORKDIR /app

ADD . /app


EXPOSE 3000


CMD ["node", "."]
